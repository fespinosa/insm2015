   $( document ).ready(function() {
      history.pushState(null, null, location.href);
      window.onpopstate = function(event) {
          history.go(1);
      };
      $('#n_fecha_nacimiento').dateEntry({dateFormat: 'dmy/'});
      $('#n_fecha_nacimientoC').dateEntry({dateFormat: 'dmy/'});

      $('#form_inscripcion').submit(function() {
        $('#btn-submit-form').hide();
        $('#error').fadeOut();
        $('#loader').fadeIn();
        
        dataform = $(this).serializeArray();
        dataform.push({ name: "es_misionero", value: $('input[name=radio_misionero]:checked').val() });
        $.ajax({
            type: "POST",
            url: "home/inscribir",
            data: dataform,
            success: function(data){
                console.log(data);
                if(data.estado == "ok"){
                  console.log(data);
                  $('#formulario_normal').fadeOut();
                  if(data.es_misionero=="Si"){
                    $('#mensaje_exito').html(data.mensaje);
                  }else{
                    $('#mensaje_exito').html(data.mensaje + data.mensaje_precio + "<br><h3>Total de su inscripción es: $" + data.precio);
                  }
                  $('#navs_formularios').hide();
                  $('#success').fadeIn();

                }else{
                  // alert("hubo un error");
                  $('#error').html(data.mensaje);
                  $('html, body').animate({scrollTop : 600},800);
                  $('#error').fadeIn();
                }
                $('#loader').hide();
                $('#btn-submit-form').fadeIn();


            }
        });
        // $('#form_inscripcion').reset();
        return false;
      });
    });

    function radio_misionero(){
      if ($('input[name=radio_misionero]:checked').val()=='Si') {
        $('#error').hide();
        //misionero on
        $("#m_zona_servicio").prop("required", true);
        $("#m_tiempo_servicio").prop("required", true);
        $("#m_descripcion_servicio").prop("required", true);
        $("#m_en_auto").prop("required", true);
        $("#m_alojamiento").prop("required", true);
        //preguntas off
        $("#n_como_supo").prop("required", false);
        $("#n_tiene_llamado").prop("required", false);
        $("#n_tiene_disposicion").prop("required", false);
        $("#n_medio_de_comunicacion").prop("required", false);

        $('.sector-misionero').fadeIn();
        $('.sector-preguntas').hide();
        $('.valores-normales').hide();

      }else{
        $('#error').hide();
        //misionero off
        $("#m_zona_servicio").prop("required", false);
        $("#m_tiempo_servicio").prop("required", false);
        $("#m_descripcion_servicio").prop("required", false);
        $("#m_en_auto").prop("required", false);
        $("#m_alojamiento").prop("required", false);
        //preguntas on
        $("#n_como_supo").prop("required", true);
        $("#n_tiene_llamado").prop("required", true);
        $("#n_tiene_disposicion").prop("required", true);
        $("#n_medio_de_comunicacion").prop("required", true);

        $('.sector-misionero').hide();        
        $('.sector-preguntas').fadeIn();
        $('.valores-normales').fadeIn();

      }

    }

    function check_dificultad(){
      if(document.getElementById('checkbox_dificultad').checked) {
        $('#n_dificultad').prop("disabled", true);
        $('#n_dificultad').prop("required", false);
        $('#n_dificultad').val("No Tengo");
      } else {
        $('#n_dificultad').prop("disabled", false);
        $('#n_dificultad').prop("required", true);
        $('#n_dificultad').val("");
      }
    }
    function check_dificultadC(){

      if(document.getElementById('checkbox_dificultadC').checked) {
        $('#n_dificultadC').prop("disabled", true);
        $('#n_dificultadC').prop("required", false);
        $('#n_dificultadC').val("No Tengo");
      } else {
        $('#n_dificultadC').prop("disabled", false);
        $('#n_dificultadC').prop("required", true);
        $('#n_dificultadC').val("");
      }
   } 
    

    function radio_conyugue(){
      if ($('input[name=inlineRadioConyugue]:checked', '#form_inscripcion').val()=='Si') {
        $("#form_conyugue").fadeIn();
        $("#n_apellidoC").prop("required", true);
        $("#checkbox_dificultadC").prop("required", true);
        $("#n_edadC").prop("required", true);
        $("#n_documentoC").prop("required", true);
        $("#n_emailC").prop("required", true);
        $("#n_fecha_nacimientoC").prop("required", true);
        $("#n_lider_ministerialC").prop("required", true);
        $("#n_nombreC").prop("required", true);
        $("#n_nacionalidadC").prop("required", true);
      }else{
        $("#form_conyugue").fadeOut();
        $("#n_apellidoC").prop("required", false);
        $("#checkbox_dificultadC").prop("required", false);
        $("#n_edadC").prop("required", false);
        $("#n_documentoC").prop("required", false);
        $("#n_emailC").prop("required", false);
        $("#n_fecha_nacimientoC").prop("required", false);
        $("#n_lider_ministerialC").prop("required", false);
        $("#n_nombreC").prop("required", false);
        $("#n_nacionalidadC").prop("required", false);
      }
    }
    function es_casado(){
      if ($( "#n_estado_civil option:selected" ).val()=="casado") {
        $('#div_radio_conyugue').fadeIn();
      }else{
        $('#div_radio_conyugue').fadeOut();
        $('input:radio[name=inlineRadioConyugue][id=radioConyugueNo]').prop('checked', true);
        $("#form_conyugue").fadeOut();

      }
    }
    function radio_hijos(){
      if ($('input[name=inlineRadioHijos]:checked', '#form_inscripcion').val()=='Si') {
        $('#n_CuantosHijos').prop("disabled", false);
        $('#n_CuantosHijos').prop("required", true);
      }else{
        $('#n_CuantosHijos').prop("disabled", true);
        $('#n_CuantosHijos').prop("required", false);
        $('#n_CuantosHijos').val("");
        $('#form_hijos').html("");
        $('#form_hijos').fadeOut();

      }
    }
    function check_dificultad_hijos(nHijo){
      if(document.getElementById('checkbox_dificultad_hijo_' + nHijo).checked) {
        $('#n_dificultad_hijo_' + nHijo).prop("disabled", true);
        $('#n_dificultad_hijo_' + nHijo).prop("required", false);
        $('#n_dificultad_hijo_' + nHijo).val("No Tiene");
      } else {
        $('#n_dificultad_hijo_' + nHijo).prop("disabled", false);
        $('#n_dificultad_hijo_' + nHijo).prop("required", true);
        $('#n_dificultad_hijo_' + nHijo).val("");
      }
    }
    function cuantosHijos(){

      $('#form_hijos').fadeIn();


      var htmlHijos = "";
      htmlHijos += "<h4>Ingrese los datos de cada uno</h4>";

      for (i = 1; i <= $('#n_CuantosHijos').val(); i++) {

        htmlHijos +='<hr>' + 
        /*Nombre*/
        '<div class="form-group">' +
          '<label for="inputNNombreHijo'+i+'" class="col-sm-2 control-label">Nombre completo niño '+i+'</label>' +
          '<div class="col-sm-8">' +
            '<input type="text" class="form-control" id="n_nombre_hijo_'+i+'" name="n_nombre_hijo_'+i+'" placeholder="Nombre completo..." required>' +
          '</div>' +
        '</div>' +
        /*Edad*/
        '<div class="form-group">' +
          '<label for="inputNEdadHijo'+i+'" class="col-sm-2 control-label">Edad niño '+i+'</label>' +
          '<div class="col-sm-8">' +
            '<select class="form-control" id="n_edad_hijo_'+i+'" name="n_edad_hijo_'+i+'" required>' + 
              '<option value="">Seleccione ...</option>';
              for (y = 1; y <= 17; y++) {
                htmlHijos+= '<option value="'+y+'">'+y+'</option>';
              }
            htmlHijos+='</select>' +
          '</div>' +
        '</div>' + 
        /*Genero*/
        '<div class="form-group">' +
          '<label for="inputNGeneroHijo_'+i+'" class="col-sm-2 control-label">Género niño '+i+'</label>' +
          '<div class="col-sm-8">' +
            '<select class="form-control" id="n_genero_hijo_'+i+'" name="n_genero_hijo_'+i+'" required>' +
              '<option value="">Seleccione ...</option>' +
              '<option value="masculino">Masculino</option>' +
              '<option value="femenino">Femenino</option>' +
            '</select>' +
          '</div>' +
        '</div>' +
        /*Dificultad fisica*/
        '<div class="form-group">' +
          '<label for="inputNDificultadHijo'+i+'" class="col-sm-2 control-label">Dificultad Física niño '+i+'</label>' +
          '<div class="col-sm-2">' +
            '<label class="checkbox-inline">' +
              '<input type="checkbox" id="checkbox_dificultad_hijo_'+i+'" name="checkbox_dificultad_hijo_'+i+'" value="" onclick="check_dificultad_hijos('+i+')" checked> No Tiene' +
            '</label>' +
          '</div>' +
          '<div class="col-sm-6">' +
            '<input type="text" class="form-control" id="n_dificultad_hijo_'+i+'" name="n_dificultad_hijo_'+i+'" placeholder="Dificultad ..." value="No Tiene" disabled>' +
          '</div>' +
        '</div>';


      }
      htmlHijos +='<hr>';
      $('#form_hijos').html(htmlHijos);

    }

    function radio_grupo(){
      if ($('input[name=inlineRadioGrupo]:checked', '#form_inscripcion').val()=='Si') {
        $('#n_Grupo').prop("disabled", false);
        $('#n_Grupo').prop("required", true);
      }else{
        $('#n_Grupo').prop("disabled", true);
        $('#n_Grupo').prop("required", false);
        $('#n_Grupo').val("");
        $('#agregarGrupo').fadeOut();
      }
    }
    function grupo(){
      if ($('#n_Grupo').val()=="otro") {
        $('#agregarGrupo').fadeIn();
        $('#n_add_grupo').prop('required', true);
      }else{
        $('#agregarGrupo').fadeOut();
        $('#n_add_grupo').prop('required', false);
      }
    }