<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		session_start();
        $this->CI =& get_instance();

	}

	public function index()
	{
		if($this->tools->repeat_group('GrupoNombre asdsad')){
			$data['existe_grupo'] = "true";
		}else{
			$data['existe_grupo'] = "false";

		}
		
		$data['grupos'] = $this->db->get('grupos')->result();
		$this->load->view('index', $data);

	}
	public function inscribir(){
		$uri = $this->CI->uri->uri_string();
        
        $params = trim(print_r($this->CI->input->post(), TRUE));
        
        log_message('debug', '==============');
        log_message('debug', 'URI: ' . $uri);
        log_message('debug', '--------------');
        log_message('debug', $params);
        log_message('debug', '==============');

        $error=false;
        /*Validación datos usuario 1*/
		$mensaje="<ul>";
		if($this->input->post('n_documento')==""||$this->input->post('n_documento')==null){$mensaje.="<li> Falta dato obligatorio: documento.</li>";$error=true;}
		if($this->input->post('n_nombre')==""||$this->input->post('n_nombre')==null){$mensaje.="<li> Falta dato obligatorio: nombre.</li>";$error=true;}
		if($this->input->post('n_apellido')==""||$this->input->post('n_apellido')==null){$mensaje.="<li> Falta dato obligatorio: apellido.</li>";$error=true;}
		if($this->input->post('n_genero')==""||$this->input->post('n_genero')==null){$mensaje.="<li> Falta dato obligatorio: genero.</li>";$error=true;}
		
		if($this->input->post('n_fecha_nacimiento')==""||$this->input->post('n_fecha_nacimiento')==null){
			$mensaje.="<li> Falta dato obligatorio: fecha de nacimiento.</li>";
			$error=true;
		}elseif(!$this->tools->valid_date($this->input->post('n_fecha_nacimiento'), 'd/m/Y')){
			$mensaje.="<li> Formato incorrecto: fecha de nacimiento.</li>";
			$error=true;
		}
		
		if($this->input->post('n_email')==""||$this->input->post('n_email')==null){
			$mensaje.="<li> Falta dato obligatorio: email.</li>";
			$error=true;
		}elseif(!valid_email($this->input->post('n_email'))){$mensaje.="<li> Formato incorrecto: email.</li>";$error=true;}
		if($this->input->post('n_telefono2')==""||$this->input->post('n_telefono2')==null){
			$mensaje.="<li> Falta dato obligatorio: telefono celular.</li>";
			$error=true;
		}elseif(!is_numeric($this->input->post('n_telefono2'))){
			$mensaje.="<li> Este dato tiene que ser numérico: telefono celular.</li>";
			$error=true;
		}
		//Validación de datos del misionero
		if($this->input->post('es_misionero')=="Si"){
			// if($this->input->post('m_zona_servicio')==""||$this->input->post('m_zona_servicio')==null){$mensaje.="<li> Falta dato obligatorio misionero: zona de servicio.</li>";$error=true;}
			if($this->input->post('m_tiempo_servicio')==""||$this->input->post('m_tiempo_servicio')==null){$mensaje.="<li> Falta dato obligatorio misionero: tiempo de servicio.</li>";$error=true;}
			if($this->input->post('m_descripcion_servicio')==""||$this->input->post('m_descripcion_servicio')==null){$mensaje.="<li> Falta dato obligatorio misionero: descripción.</li>";$error=true;}
			if($this->input->post('m_en_auto')==""||$this->input->post('m_en_auto')==null){$mensaje.="<li> Falta dato obligatorio misionero: si va en auto a Misión 2015.</li>";$error=true;}
			if($this->input->post('m_alojamiento')==""||$this->input->post('m_alojamiento')==null){$mensaje.="<li> Falta dato obligatorio misionero: facilidad de hospedaje.</li>";$error=true;}
		}elseif($this->input->post('es_misionero')=="No"){
			//Validación de preguntas
			if($this->input->post('n_como_supo')==""||$this->input->post('n_como_supo')==null){$mensaje.="<li> Falta respuesta: ¿Cómo supo de Misión 2015?.</li>";$error=true;}
			if($this->input->post('n_tiene_llamado')==""||$this->input->post('n_tiene_llamado')==null){$mensaje.="<li> Falta respuesta: ¿Tiene llamado misionero?.</li>";$error=true;}
			if($this->input->post('n_tiene_disposicion')==""||$this->input->post('n_tiene_disposicion')==null){$mensaje.="<li> Falta respuesta: ¿Tiene disposición para ser enviado?.</li>";$error=true;}
			if($this->input->post('n_medio_de_comunicacion')==""||$this->input->post('n_medio_de_comunicacion')==null){$mensaje.="<li> Falta respuesta: medio para contartarlo.</li>";$error=true;}
			
		}else{
			$mensaje.="<li> Debe elegir si es misionero o no.</li>";
			$error=true;
		}

		if($this->input->post('n_telefono1')!=""&&$this->input->post('n_telefono1')!=null&&!is_numeric($this->input->post('n_telefono1'))){$mensaje.="<li> Este dato tiene que ser numérico: telefono fijo.</li>";$error=true;}
		if($this->input->post('n_nacionalidad')==""||$this->input->post('n_nacionalidad')==null){$mensaje.="<li> Falta dato obligatorio: nacionalidad.</li>";$error=true;}
		if($this->input->post('n_pais_residencia')==""||$this->input->post('n_pais_residencia')==null){$mensaje.="<li> Falta dato obligatorio: pais de residencia.</li>";$error=true;}
		if($this->input->post('n_ciudad_residencia')==""||$this->input->post('n_ciudad_residencia')==null){$mensaje.="<li> Falta dato obligatorio: ciudad de residencia.</li>";$error=true;}
		if($this->input->post('n_organizacion')==""||$this->input->post('n_organizacion')==null){$mensaje.="<li> Falta dato obligatorio: organizacion.</li>";$error=true;}
		if($this->input->post('n_lider_ministerial')==""||$this->input->post('n_lider_ministerial')==null){$mensaje.="<li> Falta dato obligatorio: lider ministerial.</li>";$error=true;}
		if($this->input->post('n_estado_civil')	==""||$this->input->post('n_estado_civil')	==null){$mensaje.="<li> Falta dato obligatorio: estado civil.</li>";$error=true;}




		//Validación de datos del conyugue
		if($this->input->post('inlineRadioConyugue')=='Si'){
			if($this->input->post('n_documentoC')==""||$this->input->post('n_documentoC')==null){$mensaje.="<li> Falta dato obligatorio conyugue: documento.</li>";$error=true;}
			if($this->input->post('n_nombreC')==""||$this->input->post('n_nombreC')==null){$mensaje.="<li> Falta dato obligatorio conyugue: nombre.</li>";$error=true;}
			if($this->input->post('n_apellidoC')==""||$this->input->post('n_apellidoC')==null){$mensaje.="<li> Falta dato obligatorio conyugue: apellido.</li>";$error=true;}
			if($this->input->post('n_fecha_nacimientoC')==""||$this->input->post('n_fecha_nacimientoC')==null){
				$mensaje.="<li> Falta dato obligatorio conyugue: fecha de nacimiento.</li>";
				$error=true;
			}elseif(!$this->tools->valid_date($this->input->post('n_fecha_nacimiento'), 'd/m/Y')){
				$mensaje.="<li> Formato incorrecto conyugue: fecha de nacimiento.</li>";
				$error=true;
			}
			if($this->input->post('n_emailC')==""||$this->input->post('n_emailC')==null){
				$mensaje.="<li> Falta dato obligatorio: email.</li>";
				$error=true;
			}elseif(!valid_email($this->input->post('n_emailC'))){$mensaje.="<li> Formato incorrecto: email conyugue.</li>";$error=true;}
			
			if($this->input->post('n_nacionalidadC')==""||$this->input->post('n_nacionalidadC')==null){$mensaje.="<li> Falta dato obligatorio conyugue: nacionalidad.</li>";$error=true;}
			if($this->input->post('n_lider_ministerialC')==""||$this->input->post('n_lider_ministerialC')==null){$mensaje.="<li> Falta dato obligatorio conyugue: lider ministerial.</li>";$error=true;}
		}
		//Validación de datos de hijos
		if($this->input->post('inlineRadioHijos')=='Si'){
			if($this->input->post('n_CuantosHijos')!=""&&$this->input->post('n_CuantosHijos')!=null){
				for ($i=1; $i <= (int)$this->input->post('n_CuantosHijos') ; $i++) { 
					if($this->input->post('n_nombre_hijo_'.$i)==""||$this->input->post('n_nombre_hijo_'.$i)==null){$mensaje.="<li> Falta dato obligatorio niño ".$i.": nombre.</li>";$error=true;}
					if($this->input->post('n_genero_hijo_'.$i)==""||$this->input->post('n_genero_hijo_'.$i)==null){$mensaje.="<li> Falta dato obligatorio niño ".$i.": género.</li>";$error=true;}
					if($this->input->post('n_edad_hijo_'.$i)==""||$this->input->post('n_edad_hijo_'.$i)==null){
						$mensaje.="<li> Falta dato obligatorio niño ".$i.": edad.</li>";
						$error=true;
					}elseif(!is_numeric($this->input->post('n_edad_hijo_'.$i))){
						$mensaje.="<li> Este dato tiene que ser numérico: edad niño ".$i.".</li>";
						$error=true;
					}
				}

			}else{
				$mensaje.="<li> Debe determinar la cantidad de hijos o niños que asiste.</li>";
				$error=true;
			}
			
			
		}
		//Validación de datos de grupos
		if($this->input->post('inlineRadioGrupo')=='Si'){
			if($this->input->post('n_Grupo')=='otro'){
				if($this->input->post('n_add_grupo')==""||$this->input->post('n_add_grupo')==null){
					$mensaje.="<li> Falta dato obligatorio: nombre de grupo.</li>";
					$error=true;
				}elseif($this->tools->repeat_group($this->input->post('n_add_grupo'))){
					$mensaje.="<li> El nombre del grupo que ingresó ya existe.</li>";
					$error=true;
				}
			}elseif($this->input->post('n_Grupo')=="" || $this->input->post('n_Grupo')==null){
				$mensaje.="<li> Debe elegir el grupo que asiste.</li>";
				$error=true;
			}
		}




		$mensaje.="</ul>";

		if($error){
			$data = array(
				'estado' => 'error',
				'mensaje' => $mensaje
			);
		}else{
			//Si todos los datos están bien ...
			$fecha_inscrito = new DateTime('NOW');
			// echo $fecha_inscrito->format("Y-m-d H:i:s");
			log_message('debug', date("Y-m-d", strtotime(str_replace('/','-',$this->input->post('n_fecha_nacimiento')))));
        	$n_fecha_nacimiento = date("Y-m-d", strtotime(str_replace('/','-',$this->input->post('n_fecha_nacimiento'))));
			$mensaje ="";
			$mensaje_precio ="";
			$datos_ninnio;
			$error = false;
			$error_hijos = false;
			$precio;
			$precio_adulto=305;
			$precio_matrimonio=515;
			$precio_hijo=305;
			$query_max_fam = $this->db->query('select max(num_familia) as max_fami from inscritos')->row();
			$num_familia = ((int)$query_max_fam->max_fami + 1);
			$id_grupo = 0;
			if($this->input->post('inlineRadioGrupo')=='Si'){
				if($this->input->post('n_Grupo')=='otro'){
					$datos_grupo = array (
							'nombre' => $this->input->post('n_add_grupo')
						);
					$this->db->insert('grupos', $datos_grupo); 
					// echo "\nquery datos_grupo : ".$this->db->last_query();
					// echo "\nID GRUPO  : ".$this->db->insert_id();
					$id_grupo = (int)$this->db->insert_id();

				}else{
					$id_grupo = (int)$this->input->post('n_Grupo');
					// echo "\nID GRUPO  : ".$this->input->post('n_Grupo');
				}
			}

			if($this->input->post('es_misionero')=="No"){


				// n_como_supo o
				// n_tiene_llamado o 
				// n_tiene_disposicion o
				// n_se_capacita 
				// n_relacion_con_entidad
				// n_necesito_info_de
				// n_medio_de_comunicacion o
				// n_orientacion_para
				// n_quisiera_que
				
				$orientacion = "";
				$quisiera = "";
				for ($i=1; $i <=5 ; $i++) {
					$orientacion.=" -- ".$this->input->post('n_orientacion_para'.$i);
					$quisiera.=" -- ".$this->input->post('n_quisiera_que'.$i);
				}


				


				$datos_usuario1 = array(
					'documento' => $this->input->post('n_documento'),
					'nombre' => $this->input->post('n_nombre'),
					'apellido' => $this->input->post('n_apellido'),
					'genero' => $this->input->post('n_genero'),
					'fecha_nacimiento' => $n_fecha_nacimiento,
					'email' => $this->input->post('n_email'),
					'telefono_fijo' => $this->input->post('n_telefono1'),
					'telefono_celular' => $this->input->post('n_telefono2'),
					'nacionalidad' => $this->input->post('n_nacionalidad'),
					'pais_residencia' => $this->input->post('n_pais_residencia'),
					'ciudad_residencia' => $this->input->post('n_ciudad_residencia'),
					'organizacion' => $this->input->post('n_organizacion'),
					'lider_ministerial' => $this->input->post('n_lider_ministerial'),
					'n_orientacion_para' => $orientacion,
					'n_quisiera_que' => $quisiera,
					'n_como_supo' => $this->input->post('n_como_supo'),
					'n_tiene_llamado' => $this->input->post('n_tiene_llamado'),
					'n_tiene_disposicion' => $this->input->post('n_tiene_disposicion'),
					'n_se_capacita' => $this->input->post('n_se_capacita'),
					'n_relacion_con_entidad' => $this->input->post('n_relacion_con_entidad'),
					'n_necesito_info_de' => $this->input->post('n_necesito_info_de'),
					'n_medio_de_comunicacion' => $this->input->post('n_medio_de_comunicacion'),
					'dificultad' => $this->input->post('n_dificultad'),
					'estado_civil' => $this->input->post('n_estado_civil'),
					'grupo_id' => $id_grupo,
					'num_familia' => $num_familia,
					'es_ninio' => false,
					'fecha_inscrito' => $fecha_inscrito->format("Y-m-d H:i:s")
				);

				$precio = $precio_adulto;
				$mensaje_precio="<li>Un adulto : $".$precio."</li>";

			}else{

        		
				$datos_usuario1 = array(
					'documento' => $this->input->post('n_documento'),
					'nombre' => $this->input->post('n_nombre'),
					'apellido' => $this->input->post('n_apellido'),
					'genero' => $this->input->post('n_genero'),
					'fecha_nacimiento' => $n_fecha_nacimiento,
					'email' => $this->input->post('n_email'),
					'telefono_fijo' => $this->input->post('n_telefono1'),
					'telefono_celular' => $this->input->post('n_telefono2'),
					'nacionalidad' => $this->input->post('n_nacionalidad'),
					'pais_residencia' => $this->input->post('n_pais_residencia'),
					'ciudad_residencia' => $this->input->post('n_ciudad_residencia'),
					'organizacion' => $this->input->post('n_organizacion'),
					'lider_ministerial' => $this->input->post('n_lider_ministerial'),
					'dificultad' => $this->input->post('n_dificultad'),
					'estado_civil' => $this->input->post('n_estado_civil'),
					'grupo_id' => $id_grupo,
					'num_familia' => $num_familia,
					'es_ninio' => false,
					'm_zona_servicio' => $this->input->post('m_zona_servicio'),
					'm_tiempo_servicio' => $this->input->post('m_tiempo_servicio'),
					'm_descripcion_servicio' => $this->input->post('m_descripcion_servicio'),
					'm_en_auto' => $this->input->post('m_en_auto'),
					'm_alojamiento' => $this->input->post('m_alojamiento'),
					'es_misionero' => true,
					'fecha_inscrito' => $fecha_inscrito->format("Y-m-d H:i:s")
				);
				
				$precio = $precio_adulto;
				$mensaje_precio="<li>Un adulto : $".$precio."</li>";

			}

			if($this->input->post('inlineRadioConyugue')=='Si'){
				$n_fecha_nacimientoC = date("Y-m-d", strtotime(str_replace('/','-',$this->input->post('n_fecha_nacimientoC'))));
        		// $n_fecha_nacimientoC = date("Y-m-d", strtotime($this->input->post('n_fecha_nacimientoC')));

				$datos_conyugue = array(
					'documento' => $this->input->post('n_documentoC'),
					'nombre' => $this->input->post('n_nombreC'),
					'apellido' => $this->input->post('n_apellidoC'),
					'genero' => ($this->input->post('n_genero')=='masculino') ? 'femenino':'masculino',
					'fecha_nacimiento' => $n_fecha_nacimientoC,
					'email' => $this->input->post('n_emailC'),
					'telefono_fijo' => $this->input->post('n_telefono'),
					'telefono_celular' => $this->input->post('n_telefono'),
					'nacionalidad' => $this->input->post('n_nacionalidadC'),
					'pais_residencia' => $this->input->post('n_pais_residencia'),
					'ciudad_residencia' => $this->input->post('n_ciudad_residencia'),
					'organizacion' => $this->input->post('n_organizacion'),
					'lider_ministerial' => $this->input->post('n_lider_ministerialC'),
					'dificultad' => $this->input->post('n_dificultadC'),
					'estado_civil' => $this->input->post('n_estado_civil'),
					'grupo_id' => $id_grupo,
					'num_familia' => $num_familia,
					'es_ninio' => false,
					'fecha_inscrito' => $fecha_inscrito->format("Y-m-d H:i:s")
				);
				$precio = $precio_matrimonio;
				$mensaje_precio="<li>Matrimonio : $".$precio."</li>";
			}


			if($this->input->post('inlineRadioHijos')=='Si'){
				for ($i=1; $i <= (int)$this->input->post('n_CuantosHijos') ; $i++) { 
					$datos_ninnio[$i] = array(
						'nombre' => $this->input->post('n_nombre_hijo_'.$i),
						'genero' => $this->input->post('n_genero_hijo_'.$i),
						'pais_residencia' => $this->input->post('n_pais_residencia'),
						'ciudad_residencia' => $this->input->post('n_ciudad_residencia'),
						'organizacion' => $this->input->post('n_organizacion'),
						'dificultad' => $this->input->post('n_dificultad_hijo_'.$i),
						'grupo_id' => $id_grupo,
						'num_familia' => $num_familia,
						'edad' => $this->input->post('n_edad_hijo_'.$i),
						'es_ninio' => true,
						'fecha_inscrito' => $fecha_inscrito->format("Y-m-d H:i:s")
					);

					if($this->input->post('n_edad_hijo_'.$i)>=12){
						$precio+=$precio_hijo;
						$mensaje_precio.="<li>Niño ".$i." : $".$precio_hijo."</li>";
					}
				}
			}

			$n_pago = array('n_pago' => $precio);
			// array_push($datos_usuario1, $n_pago);
			$this->db->insert('inscritos', $datos_usuario1); 
			// echo "\nquery datos_usuario1 : ".$this->db->last_query();
			if($this->input->post('inlineRadioConyugue')=='Si')$this->db->insert('inscritos', $datos_conyugue);
			if($this->input->post('inlineRadioHijos')=='Si'){
				for ($i=1; $i <=count($datos_ninnio); $i++) { 
					$this->db->insert('inscritos', $datos_ninnio[$i]); 
				}
			}

			if($this->input->post('es_misionero')=="No"){
				$mensaje = "Hemos recibido sus datos con éxito, acabamos de enviar un correo a ".$this->input->post('n_email')." dando los detalles de pago para terminar el proceso de inscripción.";
				$data = array(
					'estado' => 'ok',
					'mensaje' => $mensaje,
					'mensaje_precio' => $mensaje_precio,
					'precio' => $precio
				);	
				$this->tools->send_email_trucha($this->input->post('n_nombre')." ".$this->input->post('n_apellido'), $this->input->post('n_email'), $precio, false);


			}else{
				$mensaje = "Hemos recibido su inscripción con éxito, acabamos de enviar un correo a ".$this->input->post('n_email').", por favor escribe a ventanasmision2015@gmail.com para mayor información sobre la participación de misioneros en este evento, y también para hacer válida la beca de inscripción.";
				$data = array(
					'estado' => 'ok',
					'mensaje' => $mensaje,
					'mensaje_precio' => $mensaje_precio,
					'precio' => $precio
				);
				$this->tools->send_email_trucha($this->input->post('n_nombre')." ".$this->input->post('n_apellido'), $this->input->post('n_email'), $precio, true);

			}



		}
			

		//add the header here
	    header('Content-Type: application/json');
	    echo json_encode($data);

	}

	public function view_datos(){
		
		// $data['inscritos'] = $this->db->get('inscritos')->result();

		$data['inscritos'] = $this->tools->get_inscritos();
		$this->load->view('view_datos', $data);

	}

	public function send_email(){
		// $this->tools->send_email("NOMBRE COMPLETO", "asderel.angel@gmail.com", 407, false);
		$this->tools->send_email_trucha("NOMBRE COMPLETO", "asderel.angel@gmail.com", 4074, false);
	}

	public function send_email_inscritos_uno(){

		$data['inscritos'] = $this->tools->get_inscritos_oferta_uno();
		$inscritos = $this->tools->get_inscritos_oferta_uno();
		foreach ($inscritos as $row) {
			$this->tools->send_email($row->nombre." ".$row->apellido, $row->email, $row->valor);
		}

		$this->load->view('test_inscritos', $data);
	}

	public function toExcel(){

		$data['inscritos'] = $this->tools->get_inscritos();
		$this->load->view('excel', $data);
	}




	

	




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */