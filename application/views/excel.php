<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=total_personas_inscritas.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>



<table id="example" class="table table-striped table-bordered" width="50%" role="grid" aria-describedby="example_info" cellspacing="0" style="width: 100%;white-space:nowrap;">
    <thead>
        <tr>
            <th colspan="17">DATOS PERSONALES</th>
            <th colspan="5">DATOS MINISTERIALES</th>
            <th colspan="9">PREGUNTAS</th>
            <th colspan="5">Otros</th>
        </tr>
        <tr role="row">
            <th>Id inscrito</th>
            <th>Es misionero?</th>
            <th>Es niño?</th>
            <th>Nº Documento</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Género</th>
            <th>Fecha de nacimiento</th>
            <th>Email</th>
            <th>Teléfono fijo</th>
            <th>Teléfono celular</th>
            <th>Nacionalidad</th>
            <th>País de residencia</th>
            <th>Ciudad de residencia</th>
            <th>Dificultad física</th>
            <th>Estado civil</th>
            <th>Edad</th>
            <th>Organización</th>
            <th>Liderazgo ministerial</th>
            <th>Zona de servicio misionero</th>
            <th>Tiempo que lleva en misiones</th>
            <th>Descripción del trabajo misionero</th>
            <!--Preguntas-->
            <th>¿Cómo supo de M2015?</th>
            <th>¿Tiene llamado misionero?</th>
            <th>Tiene dispocisión para ser enviado¿?</th>
            <th>¿Se capacita como misionero?</th>
            <th>¿se relaciona con alguna entidad misionera?</th>
            <th>Necesito info de</th>
            <th>Medio par contactarlo</th>
            <th>Necesito orientación para</th>
            <th>Quisiera que</th>
            <!--end-Preguntas-->
            <th>Facilidad de auto</th>
            <th>Facilidad de alojamiento</th>
            <th>Nº Familia</th>
            <th>Grupo</th>
            <th>Fecha inscrito</th>
        </tr>

    </thead>

    

    <tbody>

        <?php foreach ($inscritos as $row): ?>
            <tr>
                <td><?php echo $row->id_inscrito ?></td>
                <td><?php 
                    if($row->es_misionero==1){
                        echo "Si";
                    }else{
                        echo "No";
                    }
                ?></td>
                <td><?php 
                    if($row->es_ninio==1){
                        echo "Si";
                    }else{
                        echo "No";
                    }

                 ?></td>
                <td><?php echo $row->documento ?></td>
                <td><?php echo $row->nombre ?></td>
                <td><?php echo $row->apellido ?></td>
                <td><?php echo $row->genero ?></td>
                <td><?php echo $row->fecha_nacimiento ?></td>
                <td><?php echo $row->email ?></td>
                <td><?php echo $row->telefono_fijo ?></td>
                <td><?php echo $row->telefono_celular ?></td>
                <td><?php echo $row->nacionalidad ?></td>
                <td><?php echo $row->pais_residencia ?></td>
                <td><?php echo $row->ciudad_residencia ?></td>
                <td><?php echo $row->dificultad ?></td>
                <td><?php echo $row->estado_civil ?></td>
                <td><?php echo $row->edad ?></td>
                <td><?php echo $row->organizacion ?></td>
                <td><?php echo $row->lider_ministerial ?></td>
                <td><?php echo $row->m_zona_servicio ?></td>
                <td><?php echo $row->m_tiempo_servicio ?></td>
                <td><?php echo $row->m_descripcion_servicio ?></td>
                <td><?php echo $row->n_como_supo ?></th>
                <td><?php echo $row->n_tiene_llamado ?></th>
                <td><?php echo $row->n_tiene_disposicion ?></th>
                <td><?php echo $row->n_se_capacita ?></th>
                <td><?php echo $row->n_relacion_con_entidad ?></th>
                <td><?php echo $row->n_necesito_info_de ?></th>
                <td><?php echo $row->n_medio_de_comunicacion ?></th>
                <td><?php echo $row->n_orientacion_para ?></th>
                <td><?php echo $row->n_quisiera_que ?></th>
                <td><?php echo $row->m_en_auto ?></td>
                <td><?php echo $row->m_alojamiento ?></td>
                <td><?php echo $row->num_familia ?></td>
                <td><?php echo $row->nombre_grupo ?></td>
                <td><?php echo $row->fecha_inscrito ?></td>
            </tr>
        <?php endforeach ?>

        
    </tbody>
</table>