<!DOCTYPE html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Formulario de inscripción Mision 2015</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="assets/css/datepicker.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/site.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body role="document">

  <?php
  $paises = new Paises();
  $arrPaises = $paises->get_paises();
  ?>
   
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12" style="background-color:#5bc0de">
          <br>
        </div>
      </div>
      <hr>
          <center>
          <div class="row">
            <div class="col-sm-12">
              <img src="assets/img/logo.png" alt="..." class="img-rounded" width="70">
            </div>
          </div>
          <div class="row">
             <div class="col-sm-12">
              <h1>FORMULARIO DE INSCRIPCIÓN</h1>
            </div>
          </div>
          </center>
          <hr>
    </div>
    <div class="container" role="main">
      <!-- <div class="row" id="navs_formularios">
        <ul class="nav nav-tabs">
            <li class="active" id="li_normal" onclick="menu('normal')"><a href="#"><span class="glyphicon glyphicon-user"></span>  Formulario Normal</a></li>
            <li id="li_misionero" onclick="menu('misionero')"><a href="#"><span class="glyphicon glyphicon-plane"></span> Formulario para misioneros</a></li>
        </ul>
      </div> -->
     
      <div class="row" id="formulario_normal">
        <div class="col-sm-12">

          
            
          <div class="row">
              <div class="col-sm-12"><h3>Por favor, responda esta pregunta</h3></div>

            <div class="col-sm-3">
              <h4>¿Es(son) misionero(a/s)?</h4>
            </div>
            <div class="col-sm-4">
              <div class="btn-group">
                <label class="btn btn-info">
                  <input type="radio" name="radio_misionero" id="radio_misionero_no" value="No" onclick="radio_misionero()" checked> No
                </label>
                <label class="btn btn-info">
                  <input type="radio" name="radio_misionero" id="radio_misionero_si" value="Si" onclick="radio_misionero()" > Si
                </label>
              </div>

              
            </div>
          </div>
          
          <br>
          <div class="panel panel-primary valores-normales">
            <div class="panel-heading">
              <h3 class="panel-title">Valores de inscripción (pesos argentinos)</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-4">
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-info">Hasta el 29 de junio</li>
                    <li class="list-group-item">Individual: $205</li>
                    <li class="list-group-item">Matrimonio: $345</li>
                    <li class="list-group-item">Hijos mayores de 12 años: $205</li>
                  </ul>
                </div>
                <div class="col-sm-4">
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-info">Hasta el 20 de agosto</li>
                    <li class="list-group-item">Individual: $305</li>
                    <li class="list-group-item">Matrimonio: $515</li>
                    <li class="list-group-item">Hijos mayores de 12 años: $305</li>
                  </ul>
                </div>
                <div class="col-sm-4">
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-info">Después del 20 de agosto hasta el congreso</li>
                    <li class="list-group-item">Individual: $405</li>
                    <li class="list-group-item">Matrimonio: $585</li>
                    <li class="list-group-item">Hijos mayores de 12 años: $405</li>
                  </ul>
                </div>
              </div>
              <p><i>* El valor total se calculará después de realizar la inscripción.</i></p>


              
            </div>
          </div>

          <div class="panel panel-primary sector-misionero" style="display:none">
            <div class="panel-heading">
              <h3 class="panel-title">Valores de inscripción (pesos argentinos)</h3>
            </div>
            <div class="panel-body">
              <!-- <p><i>* El valor de la inscripción para misioneros, tanto familias como misioneros que asisten a Misión 2015 será por medio de una ofrenda.</i></p> -->
              <p><i>* Para misioneros argentinos trabajando en contexto transcultural (lengua y cultura distinta a la propia) y que han vuelto del campo despues del 01/03/2015 o salen al campo antes del 04/04/2016 podrán acceder a una beca.</i></p>
              <div class="row">
                <div class="col-sm-4">
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-info">Hasta el 29 de junio</li>
                    <li class="list-group-item">Individual: $205</li>
                    <li class="list-group-item">Matrimonio: $345</li>
                    <li class="list-group-item">Hijos mayores de 12 años: $205</li>
                  </ul>
                </div>
                <div class="col-sm-4">
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-info">Hasta el 20 de agosto</li>
                    <li class="list-group-item">Individual: $305</li>
                    <li class="list-group-item">Matrimonio: $515</li>
                    <li class="list-group-item">Hijos mayores de 12 años: $305</li>
                  </ul>
                </div>
                <div class="col-sm-4">
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-info">Después del 20 de agosto hasta el congreso</li>
                    <li class="list-group-item">Individual: $405</li>
                    <li class="list-group-item">Matrimonio: $585</li>
                    <li class="list-group-item">Hijos mayores de 12 años: $405</li>
                  </ul>
                </div>
              </div>

              <p><i>* El valor total se calculará después de realizar la inscripción.</i></p>
              <p><i>* Se han agregado campos en el formulario que es muy importante para nosotros que pueda llenar como misionero.</i></p>
            </div>
          </div>

          <h4>Ingrese sus datos de inscripción</h4>

          
          <h5> Info: datos obligatorios <span class="obli">(*)</span> </h5>
        </div>
        <div class="col-sm-12">
          <br>
          <div class="alert alert-danger" id="error" role="alert" style="display:none">
            Hay errores en el formulario.
          </div>
        </div>
        <div class="col-sm-12">
          <form class="form-horizontal" id="form_inscripcion" method="POST">
            <div class="form-group">
              <label for="inputNDocumento" class="col-sm-2 control-label">Nº Documento / Pasaporte<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_documento" name="n_documento" placeholder="Nº Documento/Pasaporte ..." required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNNombre" class="col-sm-2 control-label">Nombre<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_nombre" name="n_nombre" placeholder="Nombre ..." required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNApellido" class="col-sm-2 control-label">Apellido<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_apellido" name="n_apellido" placeholder="Apellido ..." required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNGenero" class="col-sm-2 control-label">Género<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <select class="form-control" id="n_genero" name="n_genero" required>
                  <option value="">Seleccione ...</option>                  
                  <option value="masculino">Masculino</option>                  
                  <option value="femenino">Femenino</option>                  
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNFechaNacimiento" class="col-sm-2 control-label">Fecha de nacimiento<span class="obli">(*)</span></label>
              <div class="col-sm-2">
                <div class="input-group">
                  <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                  <input type="text" class="form-control" data-date-format="dd/mm/yyyy" id="n_fecha_nacimiento" name="n_fecha_nacimiento" style="width:150px" placeholder="31/12/1980" required >
                </div>
              </div>
              <label for="inputNEmail" class="col-sm-2 control-label">Email<span class="obli">(*)</span></label>
              <div class="col-sm-4">
                <div class="input-group">
                  <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-envelope"></span></span>
                  <input type="email" class="form-control" id="n_email" name="n_email" placeholder="micorreo@mimail.com" required>
                </div>
              </div>
            </div>
              
            <div class="form-group">
              <label for="inputNtelefono1" class="col-sm-2 control-label">Teléfono fijo</label>
              <div class="col-sm-8">
                <input type="number" class="form-control" id="n_telefono1" name="n_telefono1" placeholder="123456789 (sin guiones ni espacios)">
              </div>
            </div>
            <div class="form-group">
              <label for="inputNtelefono2" class="col-sm-2 control-label">Teléfono celular<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="number" class="form-control" id="n_telefono2" name="n_telefono2" placeholder="123456789 (sin guiones ni espacios)" required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNNacionalidad" class="col-sm-2 control-label">Nacionalidad<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <select class="form-control" id="n_nacionalidad" name="n_nacionalidad" required>
                  <option value="">Seleccione ...</option>

                  <?php for ($i=0; $i < count($arrPaises); $i++) {  ?>
                  <option value="<?php echo $arrPaises[$i]; ?>"><?php echo $arrPaises[$i]; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNResidencia" class="col-sm-2 control-label">País de Residencia<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <select class="form-control" id="n_pais_residencia" name="n_pais_residencia" required>
                  <option value="">Seleccione ...</option>

                  <?php for ($i=0; $i < count($arrPaises); $i++) {  ?>
                  <option value="<?php echo $arrPaises[$i]; ?>"><?php echo $arrPaises[$i]; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNCiudadResidencia" class="col-sm-2 control-label">Ciudad de Residencia<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_cuidad_residencia" name="n_ciudad_residencia" placeholder="Ciudad Residencia ..." required>
              </div>
            </div>
            <div class="form-group sector-misionero">
              <center>
                <div class="col-sm-4">
                  <hr>
                </div>
                <div class="col-sm-2">
                  <h4>Datos de misionero</h4>
                </div>
                <div class="col-sm-4">
                  <hr>
                </div>
              </center>
            </div>
            <div class="form-group sector-misionero">
              <label for="inputMZonaServicio" class="col-sm-2 control-label">En qué país o zona está sirviendo?</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="m_zona_servicio" name="m_zona_servicio" placeholder="Zona de servicio ..." >
              </div>
            </div>
            <div class="form-group sector-misionero">
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Cuánto tiempo lleva en el campo?<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="m_tiempo_servicio" name="m_tiempo_servicio" placeholder="Tiempo ..." >
              </div>
            </div>
            <div class="form-group sector-misionero">
              <label for="inputMDescripcionServicio" class="col-sm-2 control-label">Describa su trabajo como misionero(a)<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <textarea class="form-control" rows="3" id="m_descripcion_servicio" name="m_descripcion_servicio" placeholder="Descripción servicio ..." ></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNOrganizacion" class="col-sm-2 control-label">Iglesia/Organización a la que pertenece<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_organizacion" name="n_organizacion" placeholder="Iglesia/Organización ..." required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNLiderMinisterial" class="col-sm-2 control-label">Cargo de liderazgo ministerial<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <select class="form-control" id="n_lider_ministerial" name="n_lider_ministerial" required>
                  <option value="">Seleccione ...</option>
                  <option value="Pastor">Pastor(a)</option>
                  <option value="Lider Iglesia">Lider Iglesia</option>
                  <option value="Ministerio Misiones">Ministerio Misiones</option>
                  <option value="Ministerio Niños">Ministerio Jovenes</option>
                  <option value="Ministerio Niños">Ministerio Niños</option>
                  <option value="Ministerio Damas">Ministerio Damas</option>
                  <option value="Miembro">Miembro</option>
                  <option value="Otros">Otros</option>
                </select>
              </div>
            </div>

            <div class="form-group sector-misionero">
              <label for="inputVieneAuto" class="col-sm-3 control-label">Asiste en Auto a Misión 2015?</label>
              <div class="col-sm-2">
                <label class="radio-inline">
                  <input type="radio" name="m_en_auto" id="radioAutoNo" value="No" > No
                </label>
                <label class="radio-inline">
                  <input type="radio" name="m_en_auto" id="radioAutoSi" value="Si"> Si
                </label>
              </div>
              <label for="inputMAlojamiento" class="col-sm-3 control-label">Tiene alguna posibilidad de hospedaje en Córdoba iglesia/amigos/familia?</label>
              <div class="col-sm-2">
                <label class="radio-inline">
                  <input type="radio" name="m_alojamiento" id="radioAlojamientoNo" value="No" > No
                </label>
                <label class="radio-inline">
                  <input type="radio" name="m_alojamiento" id="radioAlojamientoSi" value="Si" > Si
                </label>
              </div>
            </div>
            <div class="form-group sector-misionero">
                <div class="col-sm-10">
                  <hr>
                </div>
            </div>

            <div class="form-group sector-preguntas">
              <center>
                <div class="col-sm-4">
                  <hr>
                </div>
                <div class="col-sm-2">
                  <h4>Preguntas importantes</h4>
                </div>
                <div class="col-sm-4">
                  <hr>
                </div>
              </center>
            </div>
            <div class="form-group sector-preguntas">
              <label for="inputMZonaServicio" class="col-sm-2 control-label">Cómo supo de misión 2015?<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <select class="form-control" id="n_como_supo" name="n_como_supo" required>
                  <option value="">Seleccione ...</option>
                  <option value="Por Facebook">Por Facebook</option>
                  <option value="Por Twitter">Por Twitter</option>
                  <option value="Por una página web">Por una página web</option>
                  <option value="Por un hermano de la iglesia">Por un hermano de la iglesia</option>
                  <option value="Por un anuncio en la iglesia">Por un anuncio en la iglesia</option>
                  <option value="Por la Red Misiones Mundiales">Por la Red Misiones Mundiales</option>
                </select>
              </div>
            </div>
            <div class="form-group sector-preguntas">
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Tiene inquietud/llamado para misiones? En caso afirmativo, a que lugar, tipo de ministerio, grupo?<span class="obli">(*)</span></label>
              <div class="col-sm-3">
                <select class="form-control" id="n_tiene_llamado" name="n_tiene_llamado" required>
                  <option value="">Seleccione ...</option>
                  <option value="No">No</option>
                  <option value="Países">Países</option>
                  <option value="No alcanzados">No alcanzados</option>
                  <option value="Familia">Familia</option>
                  <option value="Comunicaciones">Comunicaciones</option>
                  <option value="Gobierno">Gobierno</option>
                  <option value="Deportes">Deportes</option>
                  <option value="Arte">Arte</option>
                  <option value="Economía">Economía</option>
                  <option value="Tecnología">Tecnología</option>
                </select>
              </div>
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Tiene disposición para ser enviado?<span class="obli">(*)</span></label>
              <div class="col-sm-3">
                <select class="form-control" id="n_tiene_disposicion" name="n_tiene_disposicion" required>
                  <option value="">Seleccione ...</option>
                  <option value="No">No</option>
                  <option value="Si">Si</option>
                  <option value="Estoy orando">Estoy orando</option>
                </select>
              </div>
            </div>
            <div class="form-group sector-preguntas">
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Está capacitándose en misiones? De qué manera: Explique brevemente.</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_se_capacita" name="n_se_capacita" placeholder="Me capacito en ..." >
              </div>
            </div>
            <div class="form-group sector-preguntas">
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Necesito orientación para:</label>
              <div class="col-sm-8">
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_orientacion_para1" name="n_orientacion_para1" value="Movilizar a la iglesia para misiones"> Movilizar a la iglesia para misiones
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_orientacion_para2" name="n_orientacion_para2" value="Mi llamado misionero"> Mi llamado misionero
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_orientacion_para3" name="n_orientacion_para3" value="Tener experiencia misionera de corto plazo"> Tener experiencia misionera de corto plazo
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_orientacion_para4" name="n_orientacion_para4" value="Saber donde capacitarme"> Saber donde capacitarme
                </label>
              </div>
            </div>
            <div class="form-group sector-preguntas">
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Quisiera que:</label>
              <div class="col-sm-8">
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_quisiera_que1" name="n_quisiera_que1" value="Visiten mi iglesia/grupo de jóvenes"> Visiten mi iglesia/grupo de jóvenes
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_quisiera_que2" name="n_quisiera_que2" value="Tengan una reunión con pastores de mi ciudad/fraternidad/asociación"> Tengan una reunión con pastores de mi ciudad/fraternidad/asociación
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_quisiera_que3" name="n_quisiera_que3" value="Realizar un congreso de misiones en mi ciudad/iglesia"> Realizar un congreso de misiones en mi ciudad/iglesia
                </label>
                <label class="checkbox-inline">
                  <input type="checkbox" id="n_quisiera_que4" name="n_quisiera_que4" value="Comenzar un grupo de oración por misiones"> Comenzar un grupo de oración por misiones
                </label>
              </div>
            </div>
            <div class="form-group sector-preguntas">
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Tiene relación actual con una entidad misionera de envío. En caso afirmativo, con quien:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_relacion_con_entidad" name="n_relacion_con_entidad" placeholder="Conozco la entidad ..." >
              </div>
            </div>
            <div class="form-group sector-preguntas">
              <label for="inputMTiempoServicio" class="col-sm-2 control-label">Necesito información acerca de: Explicar brevemente:</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="n_necesito_info_de" name="n_necesito_info_de" placeholder="Necesito info de ..." >
              </div>
              <label for="inputMTiempoServicio" class="col-sm-3 control-label">El mejor medio para comunicarse conmigo es:<span class="obli">(*)</span></label>
              <div class="col-sm-2">
                <select class="form-control" id="n_medio_de_comunicacion" name="n_medio_de_comunicacion" required>
                  <option value="">Seleccione ...</option>
                  <option value="Email">Email</option>
                  <option value="Teléfono celular">Teléfono celular</option>
                  <option value="Teléfono fijo">Teléfono fijo</option>
                </select>
              </div>
            </div>
            
            
            <div class="form-group sector-preguntas">
                <div class="col-sm-10">
                  <hr>
                </div>
            </div>

            
            <div class="form-group">
              <label for="inputNDificultad" class="col-sm-2 control-label">Dificultad Física</label>
              <div class="col-sm-2">
                
                <label class="checkbox-inline">
                  <input type="checkbox" id="checkbox_dificultad" name="checkbox_dificultad" value="" onclick="check_dificultad()" checked> No Tengo
                </label>
              </div>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="n_dificultad" name="n_dificultad" placeholder="Dificultad ..." value="No Tengo" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNEstadoCivil" class="col-sm-2 control-label">Estado Civil<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <select class="form-control" id="n_estado_civil" name="n_estado_civil" onChange="es_casado()" required>
                  <option value="">Seleccione ...</option>
                  <option value="soltero">Soltero(a)</option>
                  <option value="casado">Casado(a)</option>
                  <option value="viudo">Viudo(a)</option>
                </select>
              </div>
            </div>
            <div class="form-group" style="display:none" id="div_radio_conyugue">
              <label for="inputNConyugue" class="col-sm-2 control-label">Asiste con su conyugue?</label>
              <div class="col-sm-2">
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioConyugue" id="radioConyugueNo" value="No" onclick="radio_conyugue()" checked> No
                </label>
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioConyugue" id="radioConyugueSi" onclick="radio_conyugue()" value="Si"> Si
                </label>
              </div>
            </div>





            <div class="row alert alert-warning" id="form_conyugue" style="display:none">
              <h4>Datos Conyugue (Obligatorio)</h4>
              <div class="form-group">
              <label for="inputNDocumentoC" class="col-sm-2 control-label">Nº Documento / Pasaporte<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_documentoC" name="n_documentoC" placeholder="Nº Documento/Pasaporte ..." >
              </div>
            </div>
              <div class="form-group">
                <label for="inputNNombreC" class="col-sm-2 control-label">Nombre<span class="obli">(*)</span></label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="n_nombreC" name="n_nombreC" placeholder="Nombre ..." >
                </div>
              </div>
              <div class="form-group">
                <label for="inputNApellidoC" class="col-sm-2 control-label">Apellido<span class="obli">(*)</span></label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="n_apellidoC" name="n_apellidoC" placeholder="Apellido ..." >
                </div>
              </div>
              <div class="form-group">
                <label for="inputNFechaNacimientoC" class="col-sm-2 control-label">Fecha de nacimiento<span class="obli">(*)</span></label>
                <div class="col-sm-2">
                  <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" class="form-control" data-date-format="dd/mm/yyyy" id="n_fecha_nacimientoC" name="n_fecha_nacimientoC" style="width:150px" placeholder="31/12/1980">
                  </div>
                </div>
                <label for="inputNEmailC" class="col-sm-2 control-label">Email<span class="obli">(*)</span></label>
                <div class="col-sm-4">
                  <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><span class="glyphicon glyphicon-envelope"></span></span>
                    <input type="email" class="form-control" id="n_emailC" name="n_emailC" placeholder="correoconyugue@mail.com" >
                  </div>
                </div>
              </div>
                
              <div class="form-group">
                <label for="inputNNacionalidadC" class="col-sm-2 control-label">Nacionalidad<span class="obli">(*)</span></label>
                <div class="col-sm-8">
                  <select class="form-control" id="n_nacionalidadC" name="n_nacionalidadC" >
                    <option value="">Seleccione ...</option>

                    <?php for ($i=0; $i < count($arrPaises); $i++) {  ?>
                    <option value="<?php echo $arrPaises[$i]; ?>"><?php echo $arrPaises[$i]; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
              <label for="inputNLiderMinisterialC" class="col-sm-2 control-label">Cargo de liderazgo ministerial<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <select class="form-control" id="n_lider_ministerialC" name="n_lider_ministerialC" >
                  <option value="">Seleccione ...</option>
                  <option value="Pastor">Pastor(a)</option>
                  <option value="Lider Iglesia">Lider Iglesia</option>
                  <option value="Ministerio Misiones">Ministerio Misiones</option>
                  <option value="Ministerio Niños">Ministerio Jovenes</option>
                  <option value="Ministerio Niños">Ministerio Niños</option>
                  <option value="Ministerio Damas">Ministerio Damas</option>
                  <option value="Miembro">Miembro</option>
                  <option value="Otros">Otros</option>
                </select>
              </div>
            </div>
              <div class="form-group">
                <label for="inputNDificultadC" class="col-sm-2 control-label">Dificultad Física</label>
                <div class="col-sm-2">
                  
                  <label class="checkbox-inline">
                    <input type="checkbox" id="checkbox_dificultadC" name="checkbox_dificultadC" value="" onclick="check_dificultadC()" checked> No Tengo
                  </label>
                </div>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="n_dificultadC" name="n_dificultadC" placeholder="Dificultad ..." value="No Tengo" disabled>
                </div>
              </div>
            </div>
            <div class="form-group" id="div_radio_hijos">
              <label for="inputNHijos" class="col-sm-2 control-label">Asiste con hijos o niños menores de 18 años?<span class="obli">(*)</span></label>
              <div class="col-sm-2">
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioHijos" id="radioHijosNo" value="No" onclick="radio_hijos()" checked> No
                </label>
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioHijos" id="radioHijosSi" onclick="radio_hijos()" value="Si"> Si
                </label>
              </div>
              <label for="inputNCuantosHijos" class="col-sm-1 control-label">Cuantos?</label>
              <div class="col-sm-5">
                <select class="form-control" id="n_CuantosHijos" name="n_CuantosHijos" onChange="cuantosHijos()" disabled>
                  <option value="">Seleccione ...</option>
                  
                  <?php for ($i=1; $i <= 20 ; $i++) {?>
                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="row alert alert-warning" id="form_hijos" style="display:none">
              <h4>Ingrese los datos de cada uno</h4>
            </div>
             <div class="form-group" id="div_radio_grupo">
              <label for="inputNGrupo" class="col-sm-2 control-label">Viene en Grupo?</label>
              <div class="col-sm-2">
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioGrupo" id="radioGrupoNo" value="No" onclick="radio_grupo()" checked> No
                </label>
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioGrupo" id="radioGrupoSi" onclick="radio_grupo()" value="Si"> Si
                </label>
              </div>
              <label for="inputNNombreGrupo" class="col-sm-1 control-label">Seleccione el nombre del grupo</label>
              <div class="col-sm-5">
                <select class="form-control" id="n_Grupo" name="n_Grupo" onChange="grupo()" disabled>
                  <option value="">Seleccione ...</option>
                  
                  
                  <?php foreach ($grupos as $row): ?>
                    <option value="<?php echo $row->id_grupo ?>"><?php echo $row->nombre ?></option>
                    
                  <?php endforeach ?>

                  <option value="otro">+ Agregar otro</option>

                </select>
              </div>
            </div>
            <div class="form-group" id="agregarGrupo" style="display:none">
              <label for="inputNNombre" class="col-sm-2 control-label">Agregue el nombre del grupo<span class="obli">(*)</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="n_add_grupo" name="n_add_grupo" placeholder="Nombre Grupo ...">
              </div>
            </div>



            




            <div class="form-group">
              <div class="col-sm-offset-9 col-sm-2">
                <button id="btn-submit-form" type="submit" class="btn btn-success">Enviar Inscripción</button>
                <img src="assets/img/loader.GIF" style="display:none" width="30" id="loader">
              </div>
            </div>
            <hr>
          </form>
          <br>
          <br>
          <br>
        </div>
      </div>
      <div class="row" id="success" style="display:none">
        <br>
        <div class="alert alert-success" id="mensaje_exito" role="alert">
          Formulario enviado correctamente.
        </div>
      </div>
      <!-- <div class="row" id="formulario_misionero" style="display:none">
        <h4>Aqui va el formulario misionero</h4>
        <h5>Trabajando en eso ... :P</h5>
      </div> -->
    </div> <!-- /container -->
    <div class="container-fluid" id="footerpro">
      <div class="row">
        <div class="col-sm-3 col-sm-offset-9">
          Desarrollado por: 
          <a href="http://fnespinosa.com" target="blank_" ><img src="assets/img/logo_pro.png" width="100"></a>
        </div>
        <br>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--<script src="assets/js/bootstrap-datepicker.js"></script>-->
    <script src="assets/js/jquery.plugin.js"></script>
    <script src="assets/js/jquery.dateentry.js"></script>
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->


  </body>
</html>
  
