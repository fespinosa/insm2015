<?php
    class Tools extends CI_Model{

        function __construct(){

        }

        function valid_date($date, $format = 'dd-mm-YYYY')
        {
          $d = DateTime::createFromFormat($format, $date);
          //Check for valid date in given format
          if($d && $d->format($format) == $date) {
             return true;
          } else {
               return false;
          }
        }
        function repeat_group($nombre_grupo)
        {
          $sql = $this->db->query("SELECT id_grupo FROM grupos where nombre='".$nombre_grupo."';");
          if($sql->num_rows() > 0){
              return TRUE;
          }else{
              return FAlSE;
          }
        }

        function get_inscritos()
        {
          $sql = $this->db->query("SELECT ins.id_inscrito, ins.documento, ins.nombre, ins.apellido, ins.genero, ins.fecha_nacimiento, ins.email, ins.telefono_fijo, ins.telefono_celular, ins.nacionalidad, ins.pais_residencia, ins.ciudad_residencia, ins.organizacion, ins.lider_ministerial, ins.dificultad, ins.estado_civil, grp.nombre AS nombre_grupo, grupo_id, ins.num_familia, ins.es_ninio, ins.edad, ins.m_zona_servicio, ins.m_tiempo_servicio, ins.m_descripcion_servicio, ins.m_en_auto, ins.m_alojamiento, ins.es_misionero, ins.n_como_supo, ins.n_tiene_llamado, ins.n_tiene_disposicion, ins.n_se_capacita, ins.n_relacion_con_entidad, ins.n_necesito_info_de, ins.n_medio_de_comunicacion, ins.n_orientacion_para, ins.n_quisiera_que, ins.fecha_inscrito
                                    FROM inscritos AS ins
                                    LEFT JOIN grupos as grp ON ins.grupo_id=grp.id_grupo");
          if($sql->num_rows() > 0){
              return $sql->result();
          }else{
              return FAlSE;
          }
        }
        function send_email($nombre_completo, $toemail, $valor, $es_misionero){

          // $fromemail="mision2015inscripciones@gmail.com";
          $fromemail="inscripcion@mision2015.com";
          

          $subject = "Datos para terminar la inscripción - Misión 2015";
          $data['valor'] = $valor;
          $data['nombre_completo'] = $nombre_completo;
          $data['es_misionero'] = $es_misionero;
          $mesg = $this->load->view('email',$data,true);

          // $config=array(
          // 'charset'=>'utf-8',
          // 'mailtype' => 'html'
          // );

          // $config = array(
          //   'protocol' => 'smtp',
          //   'smtp_host' => 'ssl://smtp.googlemail.com',
          //   'smtp_port' => 465,
          //   'smtp_user' => $fromemail,
          //   'smtp_pass' => 'oraciones',
          //   'mailtype' => 'html',
          //   'charset' => 'utf-8',
          //   'wordwrap'=> TRUE
          // ); 
          $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.mision2015.com',
            'smtp_user' => $fromemail,
            'smtp_pass' => '123@Inscripcion_',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap'=> TRUE
          );    

          $this->load->library('email',$config);
          $this->email->set_newline("\r\n");
          // $this->email->initialize($config);

          $this->email->to($toemail);
          $this->email->from($fromemail, "Departamento de Tesorería M2015");
          $this->email->subject($subject);
          $this->email->message($mesg);
          $mail = $this->email->send();

          return $mail;
        }

        function send_email_trucha($nombre_completo, $toemail, $valor, $es_misionero){

          // $fromemail="mision2015inscripciones@gmail.com";
          $fromemail="inscripcion@mision2015.com";
          

          $subject = "Datos para terminar la inscripción - Misión 2015";
          $data['valor'] = $valor;
          $data['nombre_completo'] = $nombre_completo;
          $data['es_misionero'] = $es_misionero;
          $mesg = $this->load->view('email',$data,true);


          //para el envío en formato HTML 
          $headers = "MIME-Version: 1.0\r\n"; 
          $headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 

          //dirección del remitente 
          $headers .= "From: Departamento de Tesorería M2015 <inscripcion@mision2015.com>\r\n"; 

          //dirección de respuesta, si queremos que sea distinta que la del remitente 
          $headers .= "Reply-To: inscripcion@mision2015.com\r\n"; 

          //ruta del mensaje desde origen a destino 
          $headers .= "Return-path: inscripcion@mision2015.com\r\n"; 

          //direcciones que recibián copia 
          // $headers .= "Cc: maria@desarrolloweb.com\r\n"; 

          //direcciones que recibirán copia oculta 
          // $headers .= "Bcc: pepe@pepe.com,juan@juan.com\r\n"; 

          mail($toemail,$subject,$mesg,$headers);
          return true;
        }



        function get_inscritos_oferta_uno(){
          $sql = $this->db->query("select num_familia, nombre, apellido, sum(valor_inscripcion) as valor, email
                                    from inscritos
                                    where (fecha_inscrito <'2015/06/30' or fecha_inscrito is null) group by num_familia");
          if($sql->num_rows() > 0){
              return $sql->result();
          }else{
              return FAlSE;
          }
        }
        

        

    }
?>
