<?php
    class Tools_db extends CI_Model{

        function __construct(){

        }

        function get_rs_staff(){
            //$sql = "SELECT * FROM `vdi_palabras_de_vida` limit 3";
            return $this->db->get("rs_staff")->result();
        }

        function get_ordenes(){
            // return $this->db->get("rs_orden_trabajo")->result();
            $sql = $this->db->query("SELECT ord.id, ord.num_orden_trabajo,ord.codigo,ord.solicitado_por, ord.fecha_solicitud, ord.codigo_equipo, stf.nombre as 'staff' FROM rs_orden_trabajo as ord, rs_staff as stf where ord.staff_id = stf.id;");
            if($sql->num_rows() > 0){
                return $sql->result();
            }else{
                return FAlSE;
            }
        }
        function get_ordenes_filter($filter){
            // return $this->db->get("rs_orden_trabajo")->result();
            $sql = $this->db->query("SELECT ord.id, ord.num_orden_trabajo,ord.codigo,ord.solicitado_por, ord.fecha_solicitud, ord.codigo_equipo, stf.nombre as 'staff' FROM rs_orden_trabajo as ord, rs_staff as stf where ord.staff_id = stf.id and ord.solicitado_por='".$filter."' ;");
            if($sql->num_rows() > 0){
                return $sql->result();
            }else{
                return FAlSE;
            }
        }
        function get_clientes(){
            // return $this->db->get("rs_orden_trabajo")->result();
            $sql = $this->db->query("SELECT distinct solicitado_por FROM rs_orden_trabajo;");
            if($sql->num_rows() > 0){
                return $sql->result();
            }else{
                return FAlSE;
            }
        }
        function get_count_ordenes(){
            // return $this->db->get("rs_orden_trabajo")->result();
            return $this->db->count_all_results("rs_orden_trabajo");
        }
        function get_orden($id){
            return $this->db->where('id', $id)->get('rs_orden_trabajo')->row();
        }

        /////////

        function get_palabras_de_vida(){
            //$sql = "SELECT * FROM `vdi_palabras_de_vida` limit 3";
            return $this->db->order_by("pal_autor", "desc")->limit(5)->get("vdi_palabras_de_vida")->result();
        }

        function get_foto_noticias(){
            return $this->db->get("vdi_foto_noticias")->result();   
        }
        function get_foto_noticia($fotn_id){

            return $this->db->where("fotn_id",$fotn_id)->get("vdi_foto_noticias")->row();
        }
        // function comprar($id_producto, $id_usuario){
        //     $this->db->set('comp_usu_id', $id_usuario);
        //     $this->db->set('comp_pro_id', $id_producto);
        //     $a = $this->db->insert('comprados');
        //     return $this->db->insert_id($a);
        // }
        // function productoYaComprado($id_producto, $id_usuario){

        //     $sql = $this->db->where("comp_usu_id",$id_usuario)->where("comp_pro_id",$id_producto)->get("comprados");

        //     if($sql->num_rows() > 0){
        //         return TRUE;
        //     }else{
        //         return FALSE;
        //     }
        // }

        // function Compradores($id){
        //     $this->db->like('comp_pro_id', $id);
        //     $this->db->from('comprados');
        //     return $this->db->count_all_results();
        // }

        // function datosProducto($id){
        //     //$id = $this->db->escape($id);
        //     return($this->db->where("pro_id", $id)->get("producto")->row());
        // }
        // function datosVendedor($id){
        //     //$id = $this->db->escape($id);
        //     return($this->db->where("usu_id", $id)->get("usuarios")->row());
        // }
        // function productosSimilares($id){
        //     $datos_producto = $this->datosProducto($id);
        //     return($this->db->where("pro_cat_id", $datos_producto->pro_cat_id)->limit(4)->get("producto")->result());
        // }
        // function categoriaProducto($id){
        //     return $this->db->select("cat_nombre")->join("producto","producto.pro_cat_id = producto_categoria.cat_id")->where("pro_id", $id)->get("producto_categoria")->row();
        // }
        // function existeProducto($id){
        //     $producto = $this->db->where("pro_id", $id)->get("producto");
        //     if($producto->num_rows() > 0){
        //         return TRUE;
        //     }else{
        //         return FAlSE;
        //     }
        // }

    }
?>
